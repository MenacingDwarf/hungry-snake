// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interact.h"
#include "Food.generated.h"

class AFoodGenerator;

UCLASS()
class HUNGRYSNAKE_API AFood : public AActor, public IInteract
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();
	
	UPROPERTY()
	AFoodGenerator* FoodGeneratorRef;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float RewardScores;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
