// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePlayerPawn.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeActor.h"
#include "Components/InputComponent.h"

// Sets default values
ASnakePlayerPawn::ASnakePlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void ASnakePlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90.0f, 0.0f, 0.0f));
	CreateSnakeActor();
}

// Called every frame
void ASnakePlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASnakePlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("VerticalAxis", this, &ASnakePlayerPawn::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("HorizontalAxis", this, &ASnakePlayerPawn::HandlePlayerHorizontalInput);
}

void ASnakePlayerPawn::CreateSnakeActor()
{
	SnakeActorRef = GetWorld()->SpawnActor<ASnakeActor>(SnakeActorClass, FTransform());
}

void ASnakePlayerPawn::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActorRef))
	{
		if (value > 0 && SnakeActorRef->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActorRef->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActorRef->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActorRef->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void ASnakePlayerPawn::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActorRef))
	{
		if (value < 0 && SnakeActorRef->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActorRef->LastMoveDirection = EMovementDirection::LEFT;
		}
		else if (value > 0 && SnakeActorRef->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActorRef->LastMoveDirection = EMovementDirection::RIGHT;
		}
	}
}

