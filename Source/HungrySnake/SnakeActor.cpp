// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActor.h"
#include "SnakeElementBase.h"
#include "Interact.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
ASnakeActor::ASnakeActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.0f;

}

// Called when the game starts or when spawned
void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	bIsDead = false;
	AddSnakeElement(4);
	MovementSpeed = 0.8f;
	LastMoveDirection = EMovementDirection::DOWN;
	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!bIsDead)
	{
		Move();
	}
}

void ASnakeActor::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		NewSnakeElement->SnakeRef = this;
		if (ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
		else {
			NewSnakeElement->SetActorHiddenInGame(true);
		}
	}
}

void ASnakeActor::Move()
{
	FVector MovementVector(0.0,0.0,0.0);
	float MovementSpeedDelta = ElementSize;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedDelta;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeedDelta;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeedDelta;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PreviousElement = SnakeElements[i - 1];
		FVector PrevLocation = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorHiddenInGame(false);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();
}

void ASnakeActor::SnakeElementBeginOverlap(ASnakeElementBase * OverlappedBlock, AActor* Other)
{
	if (IsValid(OverlappedBlock))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedBlock, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteract* InteractInterface = Cast<IInteract>(Other);
		if (InteractInterface)
		{
			InteractInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeActor::GameOver()
{
	bIsDead = true;
}

void ASnakeActor::GetScores(float ScoresAmount)
{
	Scores += ScoresAmount;
	MovementSpeed = 0.8f - Scores * 0.04f;
	FMath::Clamp(MovementSpeed, 0.05f, 0.8f);
	SetActorTickInterval(MovementSpeed);
}

