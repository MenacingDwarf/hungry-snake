// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodGenerator.h"
#include "Math/UnrealMathUtility.h"
#include "Food.h"

// Sets default values
AFoodGenerator::AFoodGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RightUpAngle = { 450.0f, 450.0f, -70.0f };
	LeftDownAngle = { -400.0f, -450.0f, -70.0f };
}

// Called when the game starts or when spawned
void AFoodGenerator::BeginPlay()
{
	Super::BeginPlay();
	GenerateNewFood();
}

// Called every frame
void AFoodGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFoodGenerator::GenerateNewFood()
{
	float XFoodLocation = FMath::RandRange(RightUpAngle.X, LeftDownAngle.X);
	float YFoodLocation = FMath::RandRange(RightUpAngle.Y, LeftDownAngle.Y);
	float ZFoodLocation = FMath::RandRange(RightUpAngle.Z, LeftDownAngle.Z);

	FVector FoodLocation = { XFoodLocation, YFoodLocation, ZFoodLocation };
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(FoodLocation));
	NewFood->FoodGeneratorRef = this;
}

