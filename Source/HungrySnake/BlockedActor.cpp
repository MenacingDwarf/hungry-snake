// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockedActor.h"
#include "SnakeActor.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABlockedActor::ABlockedActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ABlockedActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockedActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockedActor::Interact(AActor * Interactor, bool bIsHead)
{
	auto SnakeRef = Cast<ASnakeActor>(Interactor);
	if (IsValid(SnakeRef) && bIsHead)
	{
		SnakeRef->GameOver();
	}
}

