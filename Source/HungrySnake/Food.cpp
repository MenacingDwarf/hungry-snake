// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeActor.h"
#include "FoodGenerator.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RewardScores = 1.0f;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor * Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto SnakeRef = Cast<ASnakeActor>(Interactor);
		if (IsValid(SnakeRef))
		{
			SnakeRef->GetScores(RewardScores);
			SnakeRef->AddSnakeElement();
			FoodGeneratorRef->GenerateNewFood();
			Destroy();
		}
	}
}

